#include "GameState.hpp"
#include "../Text.hpp"

GameState::GameState(SDL_Window* w, SDL_Renderer* r, TTF_Font* f){
    Text *tempText;
    ren = r;
    win = w;
    font= f;
    
    this->tGrid   = LoadImage("assets/Grid.png", ren);
    this->tSquares = LoadImage("assets/Blocks.png", ren);

    this->grid = Entity(tGrid, 0, 0, 513, 321, SCREEN_WIDTH/2 - 513/2, SCREEN_HEIGHT/2 - 321/2);
    this->aSquare = Entity(tSquares, 0,  0, SQUARE_SIZE, SQUARE_SIZE, 0, 0);
    this->bSquare = Entity(tSquares, 31, 0, SQUARE_SIZE, SQUARE_SIZE, 0, 0);
    
    Square a;
    a.type = A;
    
    Square b;
    b.type = B;
    this->activeBlock = Block(a, a, a, b);
    this->activeBlockX = 1;
    this->activeBlockY = 1;
} 

void GameState::terminate() {
    //destroy items
} 
void GameState::activate() {
} 

void GameState::deactivate() {
    // nothing is necessary 
}

FLU_StateName GameState::update() {
    SDL_Event event;
    Uint32 previousTick = SDL_GetTicks();
    Uint32 currentTick;
    
    while(true){
        
        currentTick = SDL_GetTicks();
        Uint32 detaTicks = currentTick- previousTick;
        
        
        
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: return EXIT;

                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym){
                        case SDLK_RIGHT: activeBlockX++; break;
                        case SDLK_LEFT: activeBlockX--; break;
                        case SDLK_UP: this->activeBlock = activeBlock.rotateCCW(); break;
                        case SDLK_DOWN: activeBlockY++; break;
                        case SDLK_ESCAPE: return MENU;
                    }
            }
        }

        render();
        
        previousTick = currentTick;
    }
    return GAME;
}
void GameState::render() {    
    SDL_SetRenderDrawColor(this->ren, ClearColor.r, ClearColor.g, ClearColor.b, ClearColor.a);
    SDL_RenderClear( this->ren);
    
    // render empty grid
    SDL_RenderCopy(this->ren, grid.texture, &grid.srcRect, &grid.destRect);
    
    // render activeBlock
    renderBlock(activeBlock, activeBlockX, activeBlockY);
    
    // render board
    int i;
    int j;
    for(i = 0; i < BOARD_HEIGHT; i++){
        for(j = 0; j < BOARD_WIDTH; j++) {
            renderSquare(board.squares[i][j], i, j);
        }
    }
    SDL_RenderPresent(this->ren);
}

void GameState:: renderBlock(Block b, int x, int y) {
    renderSquare(b.squares[0], x,     y);
    renderSquare(b.squares[1], x + 1, y);
    renderSquare(b.squares[2], x,     y + 1);
    renderSquare(b.squares[3], x + 1, y + 1);

}

void GameState::renderSquare(Square s, int x, int y) {
    Entity currentSquare;
            if(s.type == A) {
                    currentSquare = aSquare.moveTo(grid.destRect.x + (32*x + 1), grid.destRect.y + (32*y + 1));
                    SDL_RenderCopy(this->ren, currentSquare.texture, &currentSquare.srcRect, &currentSquare.destRect);
            } else if (s.type == B) {
                    currentSquare = bSquare.moveTo(grid.destRect.x + (32*x + 1), grid.destRect.y + (32*y + 1));
                    SDL_RenderCopy(this->ren, currentSquare.texture, &currentSquare.srcRect, &currentSquare.destRect);
            }
}
