#include "MenuState.hpp"
#include "../Text.hpp"
#include<map>

MenuState::MenuState(SDL_Window *w, SDL_Renderer *r, TTF_Font *f) {
    
    unsigned int i;
    Text *tempText;
    ren = r;
    win = w;
    font= f;

    currSelection = 0;

    // strings that represent menu items
    tempText = new Text(font, "Start Game", ren);
    stateList.push_back(GAME);
    items[GAME] = tempText;
    
    tempText = new Text(font, "High Scores", ren);
    stateList.push_back(SCORES);
    items [SCORES] = tempText;
    
    tempText = new Text(font, "Exit", ren);
    stateList.push_back(EXIT);
    items[EXIT] = tempText;

    for(i = 0; i < stateList.size(); i++) {
        items[stateList[i]]->setPosition(SCREEN_WIDTH/2, 50 + 20*i);
    }

} 

void MenuState::terminate() {
    //destroy items
} 
void MenuState::activate() {
    currSelection = 0;
} 

void MenuState::deactivate() {
    // nothing is necessary 
}

FLU_StateName MenuState::update() {
    SDL_Event event;
    while(true){
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: return EXIT;

                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym){
                        case SDLK_DOWN: currSelection = (currSelection + 1) % stateList.size(); break;
                        case SDLK_UP:   currSelection = (currSelection == 0) ? 
                                                        stateList.size() - 1 : currSelection - 1; break;
                        case SDLK_ESCAPE: return EXIT;
			default: return stateList[currSelection];
                    }
            }
        }

        render();
    }
    return MENU;
}
void MenuState::render() {
    
    int i;
    SDL_SetRenderDrawColor(this->ren, ClearColor.r, ClearColor.g, ClearColor.b, ClearColor.a);
    SDL_RenderClear(this->ren);
    for(i = 0; i < stateList.size(); i++) {


    // render as selected 
    if(i == currSelection) {
        items[stateList[i]]->render(true);
    }

    // render as unselected
    else { 
        items[stateList[i]]->render(false);
    }
    }
    SDL_RenderPresent(this->ren);
}
