#ifndef _FLUR_GAMESTATE_
#define _FLUR_GAMESTATE_
#include "State.hpp"
#include "../Entity.hpp"
#include "../BoardSystem/Board.hpp"

class GameState : public State {
    private:
        SDL_Color textColor;        
        SDL_Texture* tGrid; 
        SDL_Texture* tSquares;
        Entity grid;
        Entity aSquare;
        Entity bSquare;
        
        Board board;
        
        Block activeBlock;
        Uint32 activeBlockX;
        Uint32 activeBlockY;

    public:
        GameState(SDL_Window *w, SDL_Renderer *r, TTF_Font *f);
        ~GameState() {}
        // called by Game's Initialize and Terminate
        //void initialize(SDL_Window *w, SDL_Renderer *r);   
        void terminate();   // should free all memory associated with this state
        
        void activate();    // called once before looping in update
        void deactivate();  // called before transitioning to the next state
 
        FLU_StateName update();
        void render();
        
        void renderSquare(Square s, int x, int y);
        void renderBlock(Block b, int x, int y);

        
};

#endif
