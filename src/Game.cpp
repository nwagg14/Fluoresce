#include <iostream>
#include "Game.hpp"

/* returns 0 on success
 *         1 if SDL failed to init
 *         2 if the window or renderer 
 *           couldn't be created
 */
int Game::Initialize() {

    // initialize SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        PrintError("SDL_Init Error");
        return 1;
    }

    // initialize window, return with the value 2 on failure 
    this->win = SDL_CreateWindow("Fluoresce", 100, 100, 
        SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if(this->win == NULL) return 2;

    // initialize renderer, return with the value 3 on failure
    this->ren = SDL_CreateRenderer(this->win, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(this->ren == NULL) return 3;

    TTF_Init();

    font = TTF_OpenFont("assets/cnr.otf", 16);

    states[MENU] = new MenuState(win, ren, font);
    states[GAME] = new GameState(win, ren, font);
    // initialize states
    /* 
    MENU
    SCORES
    GAME
    END 
    PAUSE
    */

    return 0; 
}

/*  returns 0 on success
 */
int Game::Loop() {
    FLU_StateName currentStateName = MENU; 
    FLU_StateName nextStateName    = MENU;
   
    // exit the loop if the next state we want is the exit state 
    while(nextStateName != EXIT){
        
        // switch states if necessary
        if (nextStateName != currentStateName) {
            states[currentStateName]->deactivate();
            states[nextStateName]->activate();
            currentStateName = nextStateName;
        } 

        nextStateName = states[currentStateName]->update();
    }

    return 0;
}

int Game::Terminate() {

    unsigned int i;
    for(i = 0; i < FLU_NUM_STATES; i++) {
        states[i]->terminate();
    }
    
    SDL_DestroyRenderer(this->ren);
    SDL_DestroyWindow(this->win);
    SDL_Quit();
    return 0;
}
