#ifndef _FLUR_SQUARE_
#define _FLUR_SQUARE_

enum FLU_SquareType {
  EMPTY = 0,
  A,
  B
  
};

struct Square {
        FLU_SquareType type;
        bool killable;
};

#endif