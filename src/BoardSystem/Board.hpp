#ifndef _FLUR_BOARD_
#define _FLUR_BOARD_
#include "Square.hpp"
#include "Block.hpp"

#define BOARD_WIDTH 16
#define BOARD_HEIGHT 10

class Board {
    private:
        

    public:
       Square squares[BOARD_HEIGHT][BOARD_WIDTH];
       Board();
       ~Board() {}
       
       void render();
        
};

#endif