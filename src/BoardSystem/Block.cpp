#include "Block.hpp"
Block::Block(Square a, Square b, Square c, Square d) {
    squares[0] = a;
    squares[1] = b;
    squares[2] = c;
    squares[3] = d;
    killable = false;
}

Block::Block() {
    
}

Block Block::rotateCW() {
    return Block(squares[2], squares[0], squares[3], squares[1]);
}

Block Block::rotateCCW() {
    return Block(squares[1], squares[3], squares[0], squares[2]);
}