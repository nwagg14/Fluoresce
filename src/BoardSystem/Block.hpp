#ifndef _FLUR_BLOCK_
#define _FLUR_BLOCK_
#include "Square.hpp"

class Block {
    public:
        Square squares[4];
        bool killable;
        
        Block(Square a, Square b, Square c, Square d);
        Block();
        ~Block() {}
        
        Block rotateCW();
        Block rotateCCW();      
};

#endif